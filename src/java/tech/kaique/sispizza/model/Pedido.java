package tech.kaique.sispizza.model;

import java.util.Date;

public class Pedido {

    private Integer idPedido;
    private Boolean statusPedido;
    private Date dataHoraPedido;
    private char tipoPedido;
    private Integer idEntregador;
    private Integer idReserva;
    private Integer idUsuario;
    private Integer idMesa;
    private Integer idEndereco;
    private Double subTotalPedido;
    private Double descontoPedido;
    private Double totalPedido;

    private Funcionario entregador;
    private Reserva reserva;
    private Usuario usuario;
    private Mesa mesa;
    private Endereco endereco;

    public Pedido() {
    }

    public Pedido(Integer idPedido, Boolean statusPedido, Date dataHoraPedido, char tipoPedido, Integer idEntregador, Integer idReserva, Integer idUsuario, Integer idMesa, Integer idEndereco, Double subTotalPedido, Double descontoPedido, Double totalPedido, Funcionario entregador, Reserva reserva, Usuario usuario, Mesa mesa, Endereco endereco) {
        this.idPedido = idPedido;
        this.statusPedido = statusPedido;
        this.dataHoraPedido = dataHoraPedido;
        this.tipoPedido = tipoPedido;
        this.idEntregador = idEntregador;
        this.idReserva = idReserva;
        this.idUsuario = idUsuario;
        this.idMesa = idMesa;
        this.idEndereco = idEndereco;
        this.subTotalPedido = subTotalPedido;
        this.descontoPedido = descontoPedido;
        this.totalPedido = totalPedido;
        this.entregador = entregador;
        this.reserva = reserva;
        this.usuario = usuario;
        this.mesa = mesa;
        this.endereco = endereco;
    }

    public Integer getIdPedido() {
        return idPedido;
    }

    public void setIdPedido(Integer idPedido) {
        this.idPedido = idPedido;
    }

    public Boolean getStatusPedido() {
        return statusPedido;
    }

    public void setStatusPedido(Boolean statusPedido) {
        this.statusPedido = statusPedido;
    }

    public Date getDataHoraPedido() {
        return dataHoraPedido;
    }

    public void setDataHoraPedido(Date dataHoraPedido) {
        this.dataHoraPedido = dataHoraPedido;
    }

    public char getTipoPedido() {
        return tipoPedido;
    }

    public void setTipoPedido(char tipoPedido) {
        this.tipoPedido = tipoPedido;
    }

    public Integer getIdEntregador() {
        return idEntregador;
    }

    public void setIdEntregador(Integer idEntregador) {
        this.idEntregador = idEntregador;
    }

    public Integer getIdReserva() {
        return idReserva;
    }

    public void setIdReserva(Integer idReserva) {
        this.idReserva = idReserva;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Integer getIdMesa() {
        return idMesa;
    }

    public void setIdMesa(Integer idMesa) {
        this.idMesa = idMesa;
    }

    public Integer getIdEndereco() {
        return idEndereco;
    }

    public void setIdEndereco(Integer idEndereco) {
        this.idEndereco = idEndereco;
    }

    public Double getSubTotalPedido() {
        return subTotalPedido;
    }

    public void setSubTotalPedido(Double subTotalPedido) {
        this.subTotalPedido = subTotalPedido;
    }

    public Double getDescontoPedido() {
        return descontoPedido;
    }

    public void setDescontoPedido(Double descontoPedido) {
        this.descontoPedido = descontoPedido;
    }

    public Double getTotalPedido() {
        return totalPedido;
    }

    public void setTotalPedido(Double totalPedido) {
        this.totalPedido = totalPedido;
    }

    public Funcionario getEntregador() {
        return entregador;
    }

    public void setEntregador(Funcionario entregador) {
        this.entregador = entregador;
    }

    public Reserva getReserva() {
        return reserva;
    }

    public void setReserva(Reserva reserva) {
        this.reserva = reserva;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Mesa getMesa() {
        return mesa;
    }

    public void setMesa(Mesa mesa) {
        this.mesa = mesa;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

}
