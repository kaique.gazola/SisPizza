/*
 *                                __           ____ 
 *                               |  | __  ____/_   |
 *                               |  |/ / / ___\|   |
 *                               |    < / /_/  >   |
 *                               |__|_ \\___  /|___|
 *                                    \/_____/      
 * Copyright © SGP 2018-2018. Todos os Direitos Reservados.
 * Desenvolvido por: Kaique Gazola (https://kaique.tech - kaique.gazola@fatec.sp.gov.br)
 */
package tech.kaique.sispizza.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ConnectionFactory {

    public static Connection getConnection() throws Exception {
        try {
            Class.forName("org.postgresql.Driver");
            return DriverManager.getConnection("jdbc:postgresql://localhost:5432/SisPizza", "postgres", "123456");
        } catch (SQLException ex) {
            if (ex.getErrorCode() == 0) {
                createDB();
                return getConnection();
            } else {
                System.out.println("Erro na ConnectionFactory ao conectar ao banco de dados! " + ex.getMessage());
                throw ex;
            }
        }
    }

    public static void closeConnection(Connection conn, Statement stmt, ResultSet rs) throws Exception {
        close(conn, stmt, rs);
    }

    public static void closeConnection(Connection conn, Statement stmt) throws Exception {
        close(conn, stmt, null);
    }

    public static void closeConnection(Connection conn) throws Exception {
        close(conn, null, null);
    }

    public static void close(Connection conn, Statement stmt, ResultSet rs) throws Exception {
        if (conn != null) {
            conn.close();
        }
        if (stmt != null) {
            stmt.close();
        }
        if (rs != null) {
            rs.close();
        }
    }

    public static void createDB() throws Exception {
        try {
            System.out.println("Criando database SisPizza.");
            Class.forName("org.postgresql.Driver");
            Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/", "postgres", "123456");
            conn.prepareStatement("create database \"SisPizza\"").execute();
            conn.close();
            System.out.println("Database bdProduto criado com sucesso!");
            createTables();
        } catch (SQLException ex) {
            throw ex;
        }
    }

    public static void createTables() throws Exception {
        try {
            Connection conn = getConnection();
            conn.prepareStatement("create table Usuario( idUsuario serial not null primary key, nomeUsuario varchar, cpfUsuario varchar, emailUsuario varchar, senhaUsuario varchar); create table Endereco( idEndereco serial not null primary key, nomeEndereco varchar, logradouroEndereco varchar, bairroEndereco varchar, cidadeEndereco varchar, ufEndereco varchar, cepEndereco varchar, principal boolean, idUsuario integer references Usuario(idUsuario) ); create table Permissao( idPermissao serial not null primary key, descricaoPermissao varchar ); create table Funcionario( idFuncionario serial not null primary key, nomeFuncionario varchar, emailFuncionario varchar, senhaFuncionario varchar, idPermissao integer references Permissao(idPermissao) ); create table Mesa( idMesa serial not null primary key, qtdCadeirasMesa Integer, statusMesa boolean, localMesa varchar(1) ); create table Reserva( idReserva serial not null primary key, statusReserva char(1), dataHoraReserva timestamp, idUsuario integer references Usuario(idUsuario), idMesa integer references Mesa(idMesa) ); create table Produto( idProduto serial not null primary key, nomeProduto varchar, descricaoProduto varchar, valorProduto numeric, imagemProduto varchar ); create table Pedido( idPedido serial not null primary key, statusPedido boolean, dataHoraPedido timestamp, tipoPedido char(1), idEntregador integer references Funcionario(idFuncionario), idReserva integer references Reserva(idReserva), idUsuario integer references Usuario(idUsuario), idMesa integer references Mesa(idMesa), idEndereco integer references Endereco(idEndereco), subTotalPedido numeric, descontoPedido numeric, totalPedido numeric ); create table ItemPedido( idItemPedido serial not null primary key, idProduto integer references Produto(idProduto), idPedido integer references Pedido(idPedido), qtdItemPedido integer, subtotal numeric );").execute();
            conn.close();
            System.out.println("Tabela(s) criada(s) com sucesso!");
        } catch (SQLException ex) {
            throw ex;
        }
    }

}
