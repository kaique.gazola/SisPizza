package tech.kaique.sispizza.model;

public class ItemPedido {

    private Integer idItemPedido;
    private Integer idProduto;
    private Integer idPedido;
    private Integer qtdItemPedido;
    private Double subtotal;

    public ItemPedido() {
    }

    public ItemPedido(Integer idItemPedido, Integer idProduto, Integer idPedido, Integer qtdItemPedido, Double subtotal) {
        this.idItemPedido = idItemPedido;
        this.idProduto = idProduto;
        this.idPedido = idPedido;
        this.qtdItemPedido = qtdItemPedido;
        this.subtotal = subtotal;
    }

    public Integer getIdItemPedido() {
        return idItemPedido;
    }

    public void setIdItemPedido(Integer idItemPedido) {
        this.idItemPedido = idItemPedido;
    }

    public Integer getIdProduto() {
        return idProduto;
    }

    public void setIdProduto(Integer idProduto) {
        this.idProduto = idProduto;
    }

    public Integer getIdPedido() {
        return idPedido;
    }

    public void setIdPedido(Integer idPedido) {
        this.idPedido = idPedido;
    }

    public Integer getQtdItemPedido() {
        return qtdItemPedido;
    }

    public void setQtdItemPedido(Integer qtdItemPedido) {
        this.qtdItemPedido = qtdItemPedido;
    }

    public Double getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(Double subtotal) {
        this.subtotal = subtotal;
    }

}
