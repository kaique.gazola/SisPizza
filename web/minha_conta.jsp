<%------------------------------------------------------------------------------------
                                 __           ____ 
                                |  | __  ____/_   |
                                |  |/ / / ___\|   |
                                |    < / /_/  >   |
                                |__|_ \\___  /|___|
                                     \/_____/      
    Copyright © 1998-2018. Todos os Direitos Reservados.
Desenvolvido por: Kaique Gazola (https://kaique.tech - kaique.gazola@fatec.sp.gov.br)
-------------------------------------------------------------------------------------
minha_conta - Criado em  25/08/2018, 19:23:42
------------------------------------------------------------------------------------%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="pt">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="description" content="Pizzaria, Reserva, Pedido, On-line">
        <meta name="Author" content="Kaique Gazola">
        <title>SisPizza - Reserva e Pedidos Online</title>
        <link rel="icon" href="favicon.ico">
        <link rel="apple-touch-icon" href="favicon.ico">
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css"/>
        <link rel="stylesheet" href="elegant_font/style.css"/>
        <!--[if lte IE 7]>
        <script src="elegant_font/lte-ie7.js"></script><![endif]-->
        <link rel="stylesheet" href="css/magnific-popup.css">
        <link rel="stylesheet" href="css/slider-pro.css">
        <link rel="stylesheet" href="css/owl.carousel.css">
        <link rel="stylesheet" href="css/owl.theme.css">
        <link rel="stylesheet" href="css/owl.transitions.css">
        <link rel="stylesheet" href="css/animate.css">
        <link rel="stylesheet" href="elegant_font/style.css">
        <link rel="stylesheet" href="css/style.css">
        <!--[if lt IE 9]>
        <script src="js/html5shiv.min.js"></script>
        <script src="js/respond.min.js"></script>
        <script type="text/javascript" src="js/selectivizr.js"></script>
        <![endif]-->
    </head>

    <body>
        <!-- Preloader  -->
        <div class="preloader">
            <div class="status"></div>
        </div>
        <!-- Header End -->
        <header>
            <!-- Navigation Menu start-->

            <nav id="topNavNoIndex" class="navbar navbar-default main-menu minified">
                <div class="container">
                    <button class="navbar-toggler hidden-md-up float-right" type="button" data-toggle="collapse"
                            data-target="#collapsingNavbar">
                        ☰
                    </button>
                    <div class="clearfix"></div>
                    <div class="d-block mx-auto float-sm-left float-md-none">
                        <a class="navbar-brand d-inline float-left d-block" href="#slider">
                            <h1 class="no-index mb-0 mt-1">SisPizza<img id="logo" src="images/logo.ico" alt="logo" class="w-5"></h1>
                        </a>
                    </div>
                    <div class="collapse navbar-toggleable-sm" id="collapsingNavbar">
                        <ul class="nav navbar-nav">
                            <li class="">
                                <a href="index.jsp"><i class="fas fa-chevron-left"></i> Voltar ao site</a>
                            </li>
                            <li>
                                <a href="minhas_reservas.jsp">Minhas Reservas</a>
                            </li>
                            <li>
                                <a href="meus_pedidos.jsp">Meus Pedidos</a>
                            </li>
                            <li>
                                <a href="meus_dados.jsp">Meus Dados</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>

        <section id="servicos" class="info-section section-wrapper pt-5">
            <div class="container pt-5">
                <div class="row pt-5">
                    <div class="our-services mt-5 pt-3">
                        <div class="row">
                            <div class="col-md-6 col-xs-12 text-xs-center wow fadeInDown" data-wow-delay=".1s">
                                <div class="service-box align-self-center">
                                    <div class="icon">
                                        <i class="fas fa-utensils"></i>
                                        <h3>Reserva</h3>
                                    </div>

                                    <p>Escolhe sua mesa, ambiente externo ou interno, a quantidade de cadeiras e em qual horário
                                        você quer reservar.</p>
                                    
                                    <a class="btn btn-primary">Reservar agora!</a>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12 text-xs-center wow fadeInDown" data-wow-delay=".1s">
                                <div class="service-box align-self-center">
                                    <div class="icon">
                                        <i class="fas fa-truck"></i>
                                        <h3>Entrega</h3>
                                    </div>

                                    <p>Faça seu pedido e escolha entre pagar no site ou na entrega, tudo isso no conforto de um
                                        click.</p>
                                    
                                    <a class="btn btn-primary">Fazer pedido!</a>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>
            </div>
        </section>

        <footer>

            <div class="container">
                <div class="row">
                    <div class="footer-containertent">

                        <ul class="footer-social-info">
                            <li>
                                <a href="#"><i class="fab fa-facebook"></i></a>
                            </li>
                            <li>
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </li>
                            <li>
                                <a href="#"><i class="fab fa-linkedin-in"></i></a>
                            </li>
                            <li>
                                <a href="#"><i class="fab fa-youtube"></i></a>
                            </li>
                        </ul>
                        <br/><br/>
                        <p>SisPizza © 2018 - Todos os direitos reservados - Desenvolvido por <a href="https://kaique.tech">Kaique
                                Gazola</a>™.</p>
                    </div>
                </div>
            </div>
        </footer>

        <script src="js/jquery-1.11.3.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/modernizr.min.js"></script>
        <script src="js/jquery.easing.1.3.js"></script>
        <script src="js/jquery.scrollUp.min.js"></script>
        <script src="js/jquery.easypiechart.js"></script>
        <script src="js/isotope.pkgd.min.js"></script>
        <script src="js/jquery.fitvids.js"></script>
        <script src="js/jquery.stellar.min.js"></script>
        <script src="js/jquery.waypoints.min.js"></script>
        <script src="js/wow.min.js"></script>
        <script src="js/jquery.nav.js"></script>
        <script src="js/imagesloaded.pkgd.min.js"></script>
        <script src="js/smooth-scroll.min.js"></script>
        <script src="js/jquery.magnific-popup.min.js"></script>
        <script src="js/jquery.sliderPro.min.js"></script>
        <script src="js/owl.carousel.min.js"></script>
        <script src="contact/jqBootstrapValidation.js"></script>
        <script src="contact/contact_me.js"></script>
        <script src="js/custom.js"></script>

    </body>
</html>