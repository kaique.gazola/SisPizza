package tech.kaique.sispizza.model;

public class Permissao {

    private Integer idPermissao;
    private String descricaoPermissao;

    public Permissao() {
    }

    public Permissao(Integer idPermissao, String descricaoPermissao) {
        this.idPermissao = idPermissao;
        this.descricaoPermissao = descricaoPermissao;
    }

    public Integer getIdPermissao() {
        return idPermissao;
    }

    public void setIdPermissao(Integer idPermissao) {
        this.idPermissao = idPermissao;
    }

    public String getDescricaoPermissao() {
        return descricaoPermissao;
    }

    public void setDescricaoPermissao(String descricaoPermissao) {
        this.descricaoPermissao = descricaoPermissao;
    }

}
