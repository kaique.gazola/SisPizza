package tech.kaique.sispizza.dao;

import java.util.List;

public interface GenericDAO {

    public abstract boolean cadastrar(Object object);

    public abstract boolean alterar(Object object);

    public abstract boolean excluir(Object object);

    public abstract Object carregar(Object object);

    public abstract List<Object> listar();
}
