package tech.kaique.sispizza.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import tech.kaique.sispizza.model.Endereco;
import tech.kaique.sispizza.model.Usuario;
import tech.kaique.sispizza.util.ConnectionFactory;

public class UsuarioDAOImpl implements GenericDAO {

    private Connection conn;

    public UsuarioDAOImpl() {
        try {
            this.conn = ConnectionFactory.getConnection();
        } catch (Exception ex) {
            System.out.println("Erro na UsuarioDAOImpl ao iniciar conexão! Erro: " + ex.getMessage());
        }
    }

    @Override
    public boolean cadastrar(Object object) {
        Usuario usuario = (Usuario) object;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        String sql = "INSERT INTO Usuario (nomeusuario, cpfusuario, emailusuario, senhausuario) VALUES (?, ?, ?, ?)";
        try {
            stmt = conn.prepareStatement(sql, new String[]{"idusuario"});
            stmt.setString(1, usuario.getNomeUsuario());
            stmt.setString(2, usuario.getCpfUsuario());
            stmt.setString(3, usuario.getEmailUsuario());
            stmt.setString(4, usuario.getSenhaUsuario());
            stmt.execute();
            rs = stmt.getGeneratedKeys();
            if (rs.next()) {
                usuario.setIdUsuario(rs.getInt(1));
            }
            for (Endereco endereco : usuario.getEnderecoUsuariolist()) {
                endereco.getUsuario().setIdUsuario(usuario.getIdUsuario());
                EnderecoDAOImpl enderecoDAOImpl = new EnderecoDAOImpl();
                enderecoDAOImpl.cadastrar(endereco);
            }
            return true;
        } catch (SQLException ex) {
            System.out.println("Erro no UsuarioDAOImpl ao cadastrar usuario: " + ex.getMessage());
            return false;
        } finally {
            try {
                ConnectionFactory.closeConnection(conn, stmt, rs);
            } catch (Exception ex) {
                System.out.println("Erro na UsuarioDAOImpl ao fechar conexão: " + ex.getMessage());
            }
        }
    }

    @Override
    public boolean alterar(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean excluir(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object carregar(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> listar() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
