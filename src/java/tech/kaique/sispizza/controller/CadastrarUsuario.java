package tech.kaique.sispizza.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import tech.kaique.sispizza.model.Endereco;
import tech.kaique.sispizza.model.Usuario;

@WebServlet(name = "CadastrarUsuario", urlPatterns = {"/CadastrarUsuario"})
public class CadastrarUsuario extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.sendRedirect("index.jsp");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {

            String nome = request.getParameter("nome");
            String email = request.getParameter("email");
            String senha1 = request.getParameter("senha1");
            String senha2 = request.getParameter("senha2");
            String cpf = request.getParameter("cpf");
            String cep = request.getParameter("cep");
            String logradouro = request.getParameter("logradouro");
            String cidade = request.getParameter("cidade");
            String uf = request.getParameter("uf");

            if (!senha1.equals(senha2)) {
                throw new Exception("A senha e sua confirmação não combinam.");
            }

            Usuario usuario = new Usuario();
            usuario.setNomeUsuario(nome);
            usuario.setEmailUsuario(email);
            usuario.setSenhaUsuario(senha1);
            usuario.setCpfUsuario(cpf);
            Endereco endereco = new Endereco();
            endereco.setCepEndereco(cep);
            endereco.setLogradouroEndereco(logradouro);
            endereco.setCidadeEndereco(cidade);
            endereco.setUfEndereco(uf);
            endereco.setNomeEndereco("PRINCIPAL");
            endereco.setPrincipal(Boolean.TRUE);
            usuario.getEnderecoUsuariolist().add(endereco);

            
        } catch (Exception ex) {

        }

    }

}
