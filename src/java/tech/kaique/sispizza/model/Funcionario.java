package tech.kaique.sispizza.model;

public class Funcionario {

    private Integer idFuncionario;
    private String nomeFuncionario;
    private String emailFuncionario;
    private String senhaFuncionario;
    private Permissao permissaoFuncionario;

    public Funcionario() {
    }

    public Funcionario(Integer idFuncionario, String nomeFuncionario, String emailFuncionario, String senhaFuncionario, Permissao permissaoFuncionario) {
        this.idFuncionario = idFuncionario;
        this.nomeFuncionario = nomeFuncionario;
        this.emailFuncionario = emailFuncionario;
        this.senhaFuncionario = senhaFuncionario;
        this.permissaoFuncionario = permissaoFuncionario;
    }

    public Integer getIdFuncionario() {
        return idFuncionario;
    }

    public void setIdFuncionario(Integer idFuncionario) {
        this.idFuncionario = idFuncionario;
    }

    public String getNomeFuncionario() {
        return nomeFuncionario;
    }

    public void setNomeFuncionario(String nomeFuncionario) {
        this.nomeFuncionario = nomeFuncionario;
    }

    public String getEmailFuncionario() {
        return emailFuncionario;
    }

    public void setEmailFuncionario(String emailFuncionario) {
        this.emailFuncionario = emailFuncionario;
    }

    public String getSenhaFuncionario() {
        return senhaFuncionario;
    }

    public void setSenhaFuncionario(String senhaFuncionario) {
        this.senhaFuncionario = senhaFuncionario;
    }

    public Permissao getPermissaoFuncionario() {
        return permissaoFuncionario;
    }

    public void setPermissaoFuncionario(Permissao permissaoFuncionario) {
        this.permissaoFuncionario = permissaoFuncionario;
    }

}
