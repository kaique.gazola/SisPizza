package tech.kaique.sispizza.model;

import java.sql.Date;

public class Reserva {

    private Integer idReserva;
    private char statusReserva;
    private Date dataHoraReserva;
    private Integer idUsuario;
    // Mesa
    private Integer idMesa;
    private Mesa mesa;

    public Reserva() {
    }

    public Reserva(Integer idReserva, char statusReserva, Date dataHoraReserva, Integer idUsuario, Integer idMesa, Mesa mesa) {
        this.idReserva = idReserva;
        this.statusReserva = statusReserva;
        this.dataHoraReserva = dataHoraReserva;
        this.idUsuario = idUsuario;
        this.idMesa = idMesa;
        this.mesa = mesa;
    }

    public Integer getIdReserva() {
        return idReserva;
    }

    public void setIdReserva(Integer idReserva) {
        this.idReserva = idReserva;
    }

    public char getStatusReserva() {
        return statusReserva;
    }

    public void setStatusReserva(char statusReserva) {
        this.statusReserva = statusReserva;
    }

    public Date getDataHoraReserva() {
        return dataHoraReserva;
    }

    public void setDataHoraReserva(Date dataHoraReserva) {
        this.dataHoraReserva = dataHoraReserva;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Integer getIdMesa() {
        return idMesa;
    }

    public void setIdMesa(Integer idMesa) {
        this.idMesa = idMesa;
    }

    public Mesa getMesa() {
        return mesa;
    }

    public void setMesa(Mesa mesa) {
        this.mesa = mesa;
    }

}
