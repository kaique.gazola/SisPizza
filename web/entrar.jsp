<%------------------------------------------------------------------------------------
                                 __           ____ 
                                |  | __  ____/_   |
                                |  |/ / / ___\|   |
                                |    < / /_/  >   |
                                |__|_ \\___  /|___|
                                     \/_____/      
    Copyright © 1998-2018. Todos os Direitos Reservados.
Desenvolvido por: Kaique Gazola (https://kaique.tech - kaique.gazola@fatec.sp.gov.br)
-------------------------------------------------------------------------------------
minha_conta - Criado em  25/08/2018, 19:23:42
------------------------------------------------------------------------------------%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="pt">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="description" content="Pizzaria, Reserva, Pedido, On-line">
        <meta name="Author" content="Kaique Gazola">
        <title>SisPizza - Reserva e Pedidos Online</title>
        <link rel="icon" href="favicon.ico">
        <link rel="apple-touch-icon" href="favicon.ico">
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css"/>
        <link rel="stylesheet" href="elegant_font/style.css"/>
        <!--[if lte IE 7]>
        <script src="elegant_font/lte-ie7.js"></script><![endif]-->
        <link rel="stylesheet" href="css/magnific-popup.css">
        <link rel="stylesheet" href="css/slider-pro.css">
        <link rel="stylesheet" href="css/owl.carousel.css">
        <link rel="stylesheet" href="css/owl.theme.css">
        <link rel="stylesheet" href="css/owl.transitions.css">
        <link rel="stylesheet" href="css/animate.css">
        <link rel="stylesheet" href="elegant_font/style.css">
        <link rel="stylesheet" href="css/style.css">
        <!--[if lt IE 9]>
        <script src="js/html5shiv.min.js"></script>
        <script src="js/respond.min.js"></script>
        <script type="text/javascript" src="js/selectivizr.js"></script>
        <![endif]-->
    </head>

    <body>
        <!-- Preloader  -->
        <div class="preloader">
            <div class="status"></div>
        </div>
        <!-- Header End -->
        <header>
            <!-- Navigation Menu start-->

            <nav id="topNavNoIndex" class="navbar navbar-default main-menu minified">
                <div class="container">
                    <button class="navbar-toggler hidden-md-up float-right" type="button" data-toggle="collapse"
                            data-target="#collapsingNavbar">
                        ☰
                    </button>
                    <div class="clearfix"></div>
                    <div class="d-block mx-auto float-sm-left float-md-none">
                        <a class="navbar-brand d-inline float-left d-block" href="#slider">
                            <h1 class="no-index mb-0 mt-1">SisPizza<img id="logo" src="images/logo.ico" alt="logo" class="w-5"></h1>
                        </a>
                    </div>
                    <div class="collapse navbar-toggleable-sm" id="collapsingNavbar">
                        <ul class="nav navbar-nav">
                            <li class="">
                                <a href="index.jsp" tabindex="1"><i class="fas fa-chevron-left"></i> Voltar ao site</a>
                            </li>
                            <li>
                                <a href="minhas_reservas.jsp">Minhas Reservas</a>
                            </li>
                            <li>
                                <a href="meus_pedidos.jsp">Meus Pedidos</a>
                            </li>
                            <li>
                                <a href="meus_dados.jsp">Meus Dados</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>

        <section id="servicos" class="info-section section-wrapper pt-5">
            <div class="container pt-5">
                <div class="row pt-5">
                    <div class="our-services mt-5">
                        <div class="row mt-3">
                            <div class="col-md-6 col-xs-12 text-xs-center wow fadeInDown" data-wow-delay=".1s" id="entrar">
                                <div class="service-box align-self-center">
                                    <div class="icon">
                                        <i class="fas fa-sign-in-alt"></i>
                                        <h3>Já tem uma conta?</h3>
                                    </div>
                                    <form>
                                        <div class="form-group text-left">
                                            <div class="form-group col-md-12">
                                                <label for="emailEntrar">Email</label>
                                                <input type="email" class="form-control" id="emailEntrar" name="email" placeholder="Email">
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label for="senhaEntrar">Senha</label>
                                                <input type="password" class="form-control" id="senhaEntrar" name="senha" placeholder="Senha">
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-primary btn-block">Entrar</button>
                                        <button class="m-2 btn btn-link" id="naoTemConta">Não tem uma conta?</button>
                                    </form>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12 text-xs-center wow fadeInDown" data-wow-delay=".1s" id="cadastrar">
                                <div class="service-box align-self-center">
                                    <div class="icon">
                                        <i class="fas fa-user-plus"></i>
                                        <h3>É sua primeira vez aqui?</h3>
                                    </div>

                                    <div class="steps-form d-none">
                                        <div class="steps-row setup-panel">
                                            <div class="steps-step">
                                                <a href="#step-1" type="button" class="btn btn-indigo btn-circle">1</a>
                                                <p>Passo 1</p>
                                            </div>
                                            <div class="steps-step">
                                                <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                                                <p>Passo 2</p>
                                            </div>
                                            <div class="steps-step">
                                                <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
                                                <p>Passo 3</p>
                                            </div>
                                        </div>
                                    </div>

                                    <form class="text-left" role="form" action="" method="post">
                                        <div class="row setup-content" id="step-1">
                                            <div class="form-group col-md-12">
                                                <label for="nome">Nome</label>
                                                <input type="text" class="form-control" id="nome" name="nome" placeholder="Nome">
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label for="email">Email</label>
                                                <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                                            </div>
                                            <button class="btn btn-primary btn-rounded nextBtn float-right mr-3" type="button">Avançar</button>
                                        </div>
                                        <div class="row setup-content" id="step-2">
                                            <div class="form-group col-md-6">
                                                <label for="senha1">Senha</label>
                                                <input type="password" class="form-control" id="senha1" name="senha1" placeholder="Senha">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="senha2">Senha <span class="text-muted">(confirmação)</span></label>
                                                <input type="password" class="form-control" id="senha2" name="senha2" placeholder="Senha">
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label for="cpf">CPF</label>
                                                <input type="text" class="form-control" id="cpf" name="cpf" placeholder="CPF">
                                            </div>
                                            <button class="btn btn-primary btn-rounded prevBtn float-left ml-3" type="button">Voltar</button>
                                            <button class="btn btn-primary btn-rounded nextBtn float-right mr-3" type="button">Avançar</button>
                                        </div>
                                        <div class="row setup-content" id="step-3">
                                            <div class="form-group col-md-4">
                                                <label for="cep">CEP</label>
                                                <input type="text" class="form-control" id="cep" name="cep">
                                            </div>
                                            <div class="form-group col-md-8">
                                                <label for="logradouro">Rua/Logradouro</label>
                                                <input type="text" class="form-control" id="logradouro" name="logradouro">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="cidade">Cidade</label>
                                                <input type="text" class="form-control" id="cidade" name="cidade">
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label for="uf">Estado</label>
                                                <input type="text" class="form-control " id="uf" name="uf" maxlength="2">
                                            </div>
                                            <div class="col-md-6">
                                                <button class="btn btn-primary btn-rounded prevBtn btn-block" type="button">Voltar</button>
                                            </div>
                                            <div class="col-md-6">
                                                <button type="submit" class="btn btn-primary btn-block">Concluir Cadastro</button>
                                            </div>
                                        </div>
                                    </form>

                                    <button class="m-2 btn btn-link" id="temConta">Já tem uma conta?</button>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>
            </div>
        </section>

        <footer>

            <div class="container">
                <div class="row">
                    <div class="footer-containertent">

                        <ul class="footer-social-info">
                            <li>
                                <a href="#"><i class="fab fa-facebook"></i></a>
                            </li>
                            <li>
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </li>
                            <li>
                                <a href="#"><i class="fab fa-linkedin-in"></i></a>
                            </li>
                            <li>
                                <a href="#"><i class="fab fa-youtube"></i></a>
                            </li>
                        </ul>
                        <br/><br/>
                        <p>SisPizza © 2018 - Todos os direitos reservados - Desenvolvido por <a href="https://kaique.tech">Kaique
                                Gazola</a>™.</p>
                    </div>
                </div>
            </div>
        </footer>

        <script src="js/jquery-1.11.3.min.js"></script>
        <script src="js/jquery-ui.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/modernizr.min.js"></script>
        <script src="js/jquery.easing.1.3.js"></script>
        <script src="js/jquery.scrollUp.min.js"></script>
        <script src="js/jquery.easypiechart.js"></script>
        <script src="js/isotope.pkgd.min.js"></script>
        <script src="js/jquery.fitvids.js"></script>
        <script src="js/jquery.stellar.min.js"></script>
        <script src="js/jquery.waypoints.min.js"></script>
        <script src="js/wow.min.js"></script>
        <script src="js/jquery.nav.js"></script>
        <script src="js/imagesloaded.pkgd.min.js"></script>
        <script src="js/smooth-scroll.min.js"></script>
        <script src="js/jquery.magnific-popup.min.js"></script>
        <script src="js/jquery.sliderPro.min.js"></script>
        <script src="js/owl.carousel.min.js"></script>
        <script src="contact/jqBootstrapValidation.js"></script>
        <script src="contact/contact_me.js"></script>
        <script src="js/custom.js"></script>
        <script>
            $(document).ready(function () {
                var focusCadastrar = false;
                var focusEntrar = false;

                $("#temConta").click(function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                    $("#cadastrar").attr("class", "col-md-6 col-xs-12 text-xs-center wow fadeInDown");
                    $("#entrar").show("drop", {direction: "right"}, "fast");
                    $("#temConta").hide();
                    $("#naoTemConta").hide();
                    focusCadastrar = false;
                    focusEntrar = false;
                });


                $("#naoTemConta").click(function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                    $("#entrar").attr("class", "col-md-6 col-xs-12 text-xs-center wow fadeInDown");
                    $("#cadastrar").show("drop", {direction: "right"}, "fast");
                    $("#temConta").hide();
                    $("#naoTemConta").hide();
                    focusCadastrar = false;
                    focusEntrar = false;
                });

                $("#entrar input").not(":input[type=button], :input[type=submit], :input[type=reset]").on('focus', function () {
                    if (!focusEntrar) {
                        focusEntrar = true;
                        $("#entrar").attr("class", "col-md-12 col-xs-12 text-xs-center wow fadeInDown");
                        $("#cadastrar").hide("drop", {direction: "left"}, "slow");
                        $("#entrar").hide("drop", {direction: "left"}, "fast");
                        $("#entrar").show("drop", {direction: "left"}, "slow");
                        $("#naoTemConta").show()();
                    }
                });

                $("#cadastrar input").not(":input[type=button], :input[type=submit], :input[type=reset]").on('focus', function () {
                    if (!focusCadastrar) {
                        focusCadastrar = true;
                        $("#cadastrar").attr("class", "col-md-12 col-xs-12 text-xs-center wow fadeInDown");
                        $("#entrar").hide("drop", {direction: "left"}, "fast");
                        $("#cadastrar").hide("drop", {direction: "left"}, "fast");
                        $("#cadastrar").show("drop", {direction: "left"}, "slow");
                        $("#temConta").show();
                    }
                });

                $("#temConta").hide();
                $("#naoTemConta").hide();


                var navListItems = $('div.setup-panel div a'),
                        allWells = $('.setup-content'),
                        allNextBtn = $('.nextBtn'),
                        allPrevBtn = $('.prevBtn');

                allWells.hide();

                navListItems.click(function (e) {
                    e.preventDefault();
                    var $target = $($(this).attr('href')),
                            $item = $(this);

                    if (!$item.hasClass('disabled')) {
                        navListItems.removeClass('btn-indigo').addClass('btn-default');
                        $item.addClass('btn-indigo');
                        allWells.hide();
                        $target.show();
                        $target.find('input:eq(0)').focus();
                    }
                });

                allPrevBtn.click(function () {
                    var curStep = $(this).closest(".setup-content"),
                            curStepBtn = curStep.attr("id"),
                            prevStepSteps = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a");

                    prevStepSteps.removeAttr('disabled').trigger('click');
                });

                allNextBtn.click(function () {
                    var curStep = $(this).closest(".setup-content"),
                            curStepBtn = curStep.attr("id"),
                            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                            curInputs = curStep.find("input[type='text'],input[type='url']"),
                            isValid = true;

                    $(".form-group").removeClass("has-error");
                    for (var i = 0; i < curInputs.length; i++) {
                        if (!curInputs[i].validity.valid) {
                            isValid = false;
                            $(curInputs[i]).closest(".form-group").addClass("has-error");
                        }
                    }

                    if (isValid)
                        nextStepWizard.removeAttr('disabled').trigger('click');
                });

                $('div.setup-panel div a.btn-indigo').trigger('click');
            });
            $("#cadastrar").show();
            $("#entrar").show();

            function limpa_formulário_cep() {
                $("#rua").val(""), $("#bairro").val(""), $("#cidade").val(""), $("#uf").val("")
            }
            $("#cep").blur(function () {
                var a = $(this).val().replace(/\D/g, "");
                if ("" != a) {
                    /^[0-9]{8}$/.test(a) ? ($("#logradouro").val("..."), $("#cidade").val("..."), $("#uf").val("..."), $("#ibge").val("..."), $.getJSON("https://viacep.com.br/ws/" + a + "/json/?callback=?", function (a) {
                        "erro"in a ? (limpa_formulário_cep(), alert("CEP não encontrado.")) : ($("#logradouro").val(a.logradouro + " " + a.bairro), $("#cidade").val(a.localidade), $("#uf").val(a.uf))
                    })) : (limpa_formulário_cep(), alert("Formato de CEP inválido."))
                } else
                    limpa_formulário_cep()
            });

        </script>

    </body>
</html>