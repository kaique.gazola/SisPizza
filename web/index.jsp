<%------------------------------------------------------------------------------------
                                 __           ____ 
                                |  | __  ____/_   |
                                |  |/ / / ___\|   |
                                |    < / /_/  >   |
                                |__|_ \\___  /|___|
                                     \/_____/      
    Copyright © 1998-2018. Todos os Direitos Reservados.
Desenvolvido por: Kaique Gazola (https://kaique.tech - kaique.gazola@fatec.sp.gov.br)
-------------------------------------------------------------------------------------
index - Criado em  25/08/2018, 19:16:17
------------------------------------------------------------------------------------%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="pt">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="description" content="Pizzaria, Reserva, Pedido, On-line">
        <meta name="Author" content="Kaique Gazola">
        <title>SisPizza - Reserva e Pedidos Online</title>
        <link rel="icon" href="favicon.ico">
        <link rel="apple-touch-icon" href="favicon.ico">
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css"/>
        <link rel="stylesheet" href="elegant_font/style.css"/>
        <!--[if lte IE 7]>
        <script src="elegant_font/lte-ie7.js"></script><![endif]-->
        <link rel="stylesheet" href="css/magnific-popup.css">
        <link rel="stylesheet" href="css/slider-pro.css">
        <link rel="stylesheet" href="css/owl.carousel.css">
        <link rel="stylesheet" href="css/owl.theme.css">
        <link rel="stylesheet" href="css/owl.transitions.css">
        <link rel="stylesheet" href="css/animate.css">
        <link rel="stylesheet" href="elegant_font/style.css">
        <link rel="stylesheet" href="css/style.css">
        <!--[if lt IE 9]>
        <script src="js/html5shiv.min.js"></script>
        <script src="js/respond.min.js"></script>
        <script type="text/javascript" src="js/selectivizr.js"></script>
        <![endif]-->
    </head>

    <body>
        <!-- Preloader  -->
        <div class="preloader">
            <div class="status"></div>
        </div>
        <!-- Header End -->
        <header>
            <!-- Navigation Menu start-->

            <nav id="topNav" class="navbar navbar-default main-menu">
                <div class="container">
                    <button class="navbar-toggler hidden-md-up pull-right" type="button" data-toggle="collapse"
                            data-target="#collapsingNavbar">
                        ☰
                    </button>

                    <div class="d-block mx-auto">
                        <a class="navbar-brand page-scroll" href="#slider">
                            <img id="logo" src="images/logo.ico" alt="logo">
                            <h1>SisPizza</h1>
                        </a>
                    </div>
                    <div class="collapse navbar-toggleable-sm" id="collapsingNavbar">
                        <ul class="nav navbar-nav">
                            <li class="active">
                                <a href="#">Início</a>
                            </li>
                            <li>
                                <a href="#servicos">Serviços</a>
                            </li>
                            <li>
                                <a href="#cardapio">Cardápio</a>
                            </li>
                            <li>
                                <a href="#sobre">Sobre</a>
                            </li>
                            <li>
                                <a href="#contato">Contato</a>
                            </li>
                            <li>
                                <a href="minha_conta.jsp">Minha Conta</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>


        </header>
        <!-- Header End -->


        <section class="slider-pro slider" id="slider">
            <div class="sp-slides">

                <!-- Slides -->
                <div class="sp-slide main-slides">
                    <div class="img-overlay"></div>

                    <img class="sp-image" src="images/slider/slider-img-1.jpg" alt="Slider 1"/>

                    <h1 class="sp-layer slider-text-big"
                        data-position="center" data-show-transition="left" data-hide-transition="right" data-show-delay="1500"
                        data-hide-delay="200">
                        <span class="highlight-texts">Faça seu pedido on-line!</span>
                    </h1>

                    <p class="sp-layer"
                       data-position="center" data-vertical="15%" data-show-delay="2000" data-hide-delay="200"
                       data-show-transition="left" data-hide-transition="right">
                        E receba no conforto da sua residência.
                    </p>
                </div>
                <!-- Slides End -->

                <!-- Slides -->
                <div class="sp-slide main-slides">
                    <div class="img-overlay"></div>
                    <img class="sp-image" src="images/slider/slider-img-2.jpg" alt="Slider 2"/>

                    <h1 class="sp-layer slider-text-big"
                        data-position="center" data-show-transition="left" data-hide-transition="right" data-show-delay="1500"
                        data-hide-delay="200">
                        <span class="highlight-texts">Reserve sua mesa on-line!</span>
                    </h1>

                    <p class="sp-layer"
                       data-position="center" data-vertical="15%" data-show-delay="2000" data-hide-delay="200"
                       data-show-transition="left" data-hide-transition="right">
                        Ignore filas e transtornos!
                    </p>
                </div>
                <!-- Slides End -->

                <!-- Slides -->
                <div class="sp-slide main-slides">
                    <div class="img-overlay"></div>

                    <img class="sp-image" src="images/slider/slider-img-3.jpg" alt="Slider 3"/>

                    <h1 class="sp-layer slider-text-big"
                        data-position="center" data-show-transition="left" data-hide-transition="right" data-show-delay="1500"
                        data-hide-delay="200">
                        <span class="highlight-texts">Verifique nosso cardápio!</span>
                    </h1>

                    <p class="sp-layer"
                       data-position="center" data-vertical="15%" data-show-delay="2000" data-hide-delay="200"
                       data-show-transition="left" data-hide-transition="right">
                        Veja todos os nossos produtos antes mesmo de ir.
                    </p>
                </div>
                <!-- Slides End -->

            </div>
        </section>
        <!-- Main Slider End -->


        <section id="servicos" class="info-section section-wrapper">
            <div class="container">
                <div class="row">

                    <!-- Section Header -->
                    <div class="col-md-12 col-sm-12 col-xs-12 section-header wow fadeInDown">
                        <h2><span class="highlight-text">Serviços</span></h2>

                        <p class="col-md-8 col-sm-10 col-xs-12 col-md-offset-2 col-sm-offset-1">Personalizar cada vez mais
                            nossos serviços com conforto e ótimo atendimento em uma única casa, tendo em vista a total
                            satisfação de nossos clientes, antevendo seus desejos e aspirações.</p>
                    </div>
                    <!-- Section Header End -->

                    <div class="our-services">
                        <div class="row">

                            <div class="col-md-4 col-sm-4 col-xs-12 text-xs-center wow fadeInDown" data-wow-delay=".1s">
                                <div class="service-box align-self-center">
                                    <div class="icon">
                                        <i class="fas fa-utensils"></i>
                                        <h3>Reserva</h3>
                                    </div>

                                    <p>Escolhe sua mesa, ambiente externo ou interno, a quantidade de cadeiras e em qual horário
                                        você quer reservar.</p>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12 text-xs-center wow fadeInDown" data-wow-delay=".1s">
                                <div class="service-box">
                                    <div class="icon">
                                        <i class="fas fa-money-check-alt"></i>
                                        <h3>Facilidade</h3>
                                    </div>

                                    <p>Aceitamos todas as formas de pagamento que o mercado oferece.</p>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12 text-xs-center wow fadeInDown" data-wow-delay=".1s">
                                <div class="service-box align-self-center">
                                    <div class="icon">
                                        <i class="fas fa-truck"></i>
                                        <h3>Entrega</h3>
                                    </div>

                                    <p>Faça seu pedido e escolha entre pagar no site ou na entrega, tudo isso no conforto de um
                                        click.</p>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>
            </div>
        </section>


        <section class="menus style3 txt-color" id="cardapio">
            <div class="container">
                <div class="row">

                    <!-- Section Header -->
                    <div class="col-md-12 col-sm-12 col-xs-12 section-header wow fadeInDown">
                        <h2><span class="highlight-text">Cardápio</span></h2>

                        <p class="col-md-8 col-sm-10 col-xs-12 col-md-offset-2 col-sm-offset-1">Sabores inconfundíveis estão
                            presentes no nosso cardápio, veja abaixo todas as maravilhas que estão disponíveis para você.</p>
                    </div>
                    <!-- Section Header End -->

                    <div class="menus-container">
                        <!-- menu -->
                        <div class="menu row">
                            <div class="col-md-6 wow fadeInRight animated"
                                 style="visibility: visible; animation-name: fadeInRight;">
                                <div class="menu-column">
                                    <div class="food">
                                        <div class="food-desc">
                                            <h6 class="food-name">Pizza Vegana</h6>
                                            <div class="dots"></div>
                                            <div class="food-price">
                                                <span>R$500,00</span>
                                            </div>
                                        </div>
                                        <div class="food-details">
                                            <span>contém carne</span>
                                        </div>
                                    </div>
                                    <div class="food">
                                        <div class="food-desc">
                                            <h6 class="food-name">Pizza Normal</h6>
                                            <div class="dots"></div>
                                            <div class="food-price">
                                                <span>R$16.00</span>
                                            </div>
                                        </div>
                                        <div class="food-details">
                                            <span>com pimenta do reino</span>
                                        </div>
                                    </div>
                                </div><!-- /menu-column -->
                            </div><!-- /col-md-6 -->
                            <div class="col-md-6 wow fadeInLeft animated"
                                 style="visibility: visible; animation-name: fadeInLeft;">
                                <div class="menu-column">
                                    <div class="food">
                                        <div class="food-desc">
                                            <h6 class="food-name">Pizza Vegana</h6>
                                            <div class="dots"></div>
                                            <div class="food-price">
                                                <span>R$500,00</span>
                                            </div>
                                        </div>
                                        <div class="food-details">
                                            <span>contém carne</span>
                                        </div>
                                    </div>
                                    <div class="food">
                                        <div class="food-desc">
                                            <h6 class="food-name">Pizza Normal</h6>
                                            <div class="dots"></div>
                                            <div class="food-price">
                                                <span>R$16.00</span>
                                            </div>
                                        </div>
                                        <div class="food-details">
                                            <span>com pimenta do reino</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section id="sobre" class="info-section section-wrapper description txt-color">
            <div class="container">
                <div class="row">
                    <!-- Section Header -->
                    <div class="col-md-12 col-sm-12 col-xs-12 section-header wow fadeInDown">
                        <h2><span class="highlight-text">Sobre nós</span></h2>

                        <p class="col-md-8 col-sm-10 col-xs-12 col-md-offset-2 col-sm-offset-1 h5 p-3">A SisPizza é hoje um dos
                            ícones
                            da gastronomia online do Brasil. Localizada em um dos bairros mais nobres de Jales, na grande São
                            Paulo, dispõe de um amplo ambiente, com mais de 500 lugares distribuídos em dois pisos, ideal para
                            reuniões familiares, encontro entre amigos e eventos. Além de sua beleza arquitetônica, a casa conta
                            com um excelente atendimento, servindo os melhores e mais nobres cortes e as mais saborosas pizzas.
                            Possui um amplo estacionamento, garantindo conforto e tranquilidade para seus clientes.
                        </p>

                        <div class="row mt-5">

                            <div class="col-md-6 mt-5">
                                <strong class="h4">Missão</strong>
                                <p>"Fidelizar clientes e utilizar produtos de qualidade proporcionando opções de refeições
                                    requintadas e saudáveis."</p>
                            </div>
                            <div class="col-md-6 mt-5">
                                <strong class="h4">Valores</strong>
                                <ul>
                                    <li>- Satisfação do cliente, dos colaboradores e dos parceiros.</li>
                                    <li>- Qualidade, variedade e agilidade.</li>
                                    <li>- Sustentabilidade.</li>
                                </ul>
                            </div>

                        </div>

                    </div>

                </div>
            </div>
        </section>

        <section id="contato" class="section-wrapper contact-section txt-color" data-stellar-background-ratio="0.5">
            <div class="parallax-overlay"></div>
            <div class="container">
                <div class="row">

                    <!-- Section Header -->
                    <div class="col-md-12 col-sm-12 col-xs-12 section-header wow fadeInDown">
                        <h2><span class="highlight-text">Contato</span></h2>

                        <p class="col-md-8 col-sm-10 col-xs-12 col-md-offset-2 col-sm-offset-1">Nós amamos o feedback, faça
                            críticas, sugestões etc, basta preencher o formulário abaixo e entraremos em contato o mais breve
                            possível.
                        </p>
                    </div>
                    <!-- Section Header End -->

                    <div class="contact-details">


                        <!-- Contact Form -->
                        <div class="contact-form wow bounceInRight">

                            <!--NOTE: Update your email Id in "contact_me.php" file in order to receive emails from your contact form-->
                            <form name="sentMessage" id="contactForm" novalidate>
                                <div class="col-md-6 offset-md-6">
                                    <input type="text" class="form-control"
                                           placeholder="Seu nome" id="nome" required
                                           data-validation-required-message="Por favor, insira o seu nome."/>
                                    <p class="help-block"></p>
                                </div>
                                <div class="col-md-6">
                                    <input type="email" class="form-control" placeholder="Email"
                                           id="email" required
                                           data-validation-required-message="Por favor, insira seu e-mail."/>
                                </div>

                                <div class="col-md-12">
                                    <textarea rows="10" cols="100" class="form-control"
                                              placeholder="Mensagem" id="mensagem" required
                                              data-validation-required-message="Por favor, insira sua mensagem." minlength="10"
                                              data-validation-minlength-message="Mínimo de 10 caracteres."
                                              maxlength="999" style="resize:none"></textarea>
                                </div>

                                <div class="col-md-8 col-md-offset-2"><br>
                                    <div id="success"></div>
                                    <button type="submit" class="btn btn-primary">Enviar</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </section>


        <section class="footer-container">
            <div class="container-fluid">
                <div class="row">
                    <div class="embed-responsive embed-responsive-21by9">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2646.405355353548!2d-50.54169081217354!3d-20.2761490568101!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x949828ccfaa94d1d%3A0x8b69addbada7ff85!2sFaculdade+de+Tecnologia+de+Jales+-+Prof.+Jos%C3%A9+Camargo!5e0!3m2!1spt-BR!2sbr!4v1535234480173"></iframe>
                    </div>
                </div>
            </div>
        </section>

        <footer>

            <div class="container">
                <div class="row">
                    <div class="footer-containertent">

                        <ul class="footer-social-info">
                            <li>
                                <a href="#"><i class="fab fa-facebook"></i></a>
                            </li>
                            <li>
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </li>
                            <li>
                                <a href="#"><i class="fab fa-linkedin-in"></i></a>
                            </li>
                            <li>
                                <a href="#"><i class="fab fa-youtube"></i></a>
                            </li>
                        </ul>
                        <br/><br/>
                        <p>SisPizza © 2018 - Todos os direitos reservados - Desenvolvido por <a href="https://kaique.tech">Kaique
                                Gazola</a>™.</p>
                    </div>
                </div>
            </div>
        </footer>

        <script src="js/jquery-1.11.3.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/modernizr.min.js"></script>
        <script src="js/jquery.easing.1.3.js"></script>
        <script src="js/jquery.scrollUp.min.js"></script>
        <script src="js/jquery.easypiechart.js"></script>
        <script src="js/isotope.pkgd.min.js"></script>
        <script src="js/jquery.fitvids.js"></script>
        <script src="js/jquery.stellar.min.js"></script>
        <script src="js/jquery.waypoints.min.js"></script>
        <script src="js/wow.min.js"></script>
        <script src="js/jquery.nav.js"></script>
        <script src="js/imagesloaded.pkgd.min.js"></script>
        <script src="js/smooth-scroll.min.js"></script>
        <script src="js/jquery.magnific-popup.min.js"></script>
        <script src="js/jquery.sliderPro.min.js"></script>
        <script src="js/owl.carousel.min.js"></script>
        <script src="contact/jqBootstrapValidation.js"></script>
        <script src="contact/contact_me.js"></script>
        <script src="js/custom.js"></script>

    </body>
</html>