package tech.kaique.sispizza.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import tech.kaique.sispizza.model.Endereco;
import tech.kaique.sispizza.util.ConnectionFactory;

public class EnderecoDAOImpl implements GenericDAO {

    private Connection conn;

    public EnderecoDAOImpl() {
        try {
            this.conn = ConnectionFactory.getConnection();
        } catch (Exception ex) {
            System.out.println("Erro na EnderecoDAOImpl ao iniciar conexão! Erro: " + ex.getMessage());
        }
    }

    @Override
    public boolean cadastrar(Object object) {
        Endereco endereco = (Endereco) object;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        String sql = "INSERT INTO Endereco(nomeendereco, logradouroendereco, bairroendereco, cidadeendereco, ufendereco, cependereco, principal, idusuario) "
                + "values (?,?,?,?,?,?,?,?,?)";
        try {
            stmt = conn.prepareStatement(sql, new String[]{"idendereco"});
            stmt.setString(1, endereco.getNomeEndereco());
            stmt.setString(2, endereco.getLogradouroEndereco());
            stmt.setString(3, endereco.getBairroEndereco());
            stmt.setString(4, endereco.getCidadeEndereco());
            stmt.setString(5, endereco.getUfEndereco());
            stmt.setString(6, endereco.getCepEndereco());
            stmt.setBoolean(7, endereco.getPrincipal());
            stmt.setInt(8, endereco.getUsuario().getIdUsuario());
            stmt.execute();
            rs = stmt.getGeneratedKeys();
            if (rs.next()) {
                endereco.setIdEndereco(rs.getInt(1));
            }
            return true;
        } catch (SQLException ex) {
            System.out.println("Erro no EnderecoDAOImpl ao cadastrar endereco: " + ex.getMessage());
            return false;
        } finally {
            try {
                ConnectionFactory.closeConnection(conn, stmt, rs);
            } catch (Exception ex) {
                System.out.println("Erro na EnderecoDAOImpl ao fechar conexão: " + ex.getMessage());
            }
        }
    }

    @Override
    public boolean alterar(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean excluir(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object carregar(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> listar() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
