package tech.kaique.sispizza.model;

public class Mesa {

    private Integer idMesa;
    private Integer qtdCadeirasMesa;
    private Boolean statusMesa;
    private char localMesa;

    public Mesa() {
    }

    public Mesa(Integer idMesa, Integer qtdCadeirasMesa, Boolean statusMesa, char localMesa) {
        this.idMesa = idMesa;
        this.qtdCadeirasMesa = qtdCadeirasMesa;
        this.statusMesa = statusMesa;
        this.localMesa = localMesa;
    }

    public Integer getIdMesa() {
        return idMesa;
    }

    public void setIdMesa(Integer idMesa) {
        this.idMesa = idMesa;
    }

    public Integer getQtdCadeirasMesa() {
        return qtdCadeirasMesa;
    }

    public void setQtdCadeirasMesa(Integer qtdCadeirasMesa) {
        this.qtdCadeirasMesa = qtdCadeirasMesa;
    }

    public Boolean getStatusMesa() {
        return statusMesa;
    }

    public void setStatusMesa(Boolean statusMesa) {
        this.statusMesa = statusMesa;
    }

    public char getLocalMesa() {
        return localMesa;
    }

    public void setLocalMesa(char localMesa) {
        this.localMesa = localMesa;
    }

}
