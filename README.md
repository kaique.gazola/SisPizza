![SisPizza](resources/Logo.png)

Introdução
----------------------------

**SisPizza** é um projeto de sistema web voltado para controle de um comércio de pizza(s).  

O app é desenvolvido usando:

- [Java EE](https://www.oracle.com/technetwork/java/javaee/tech/index.html) - Série de especificações Java.
- [PostgreSQL](https://www.postgresql.org/) - Sistema de Gerencimaneto de Banco de Dados de Objeto-Relacional de código aberto.
- [Bootstrap 4](https://getbootstrap.com) - Popular framework para desenvolvimento Front-End.

Wireframe's
----------------------------

### - Geral - Usuário

`Inicial` - https://wireframe.cc/yXuH2q
---
`Entrar/Registrar` - https://wireframe.cc/bjopFw
---
`Dashboard - Minha Conta` - https://wireframe.cc/QD8SeM
---
`Minhas Reservas` - https://wireframe.cc/pKhoPk
---
`Meus Pedidos` - https://wireframe.cc/7kCtom
---
`Minha Conta` - https://wireframe.cc/kFuU1d
---
`Reservar` - https://wireframe.cc/RnAv1w `(Usuário / Área Restrita)`
---
`Novo pedido` - https://wireframe.cc/2kAorb `(Usuário / Área Restrita)`
---
`Carrinho` - https://wireframe.cc/vE0qcJ
---

### - Área Restrita - Admin

`Listar Reservas` - https://wireframe.cc/1cADu1
---
`Listar Pedidos` - https://wireframe.cc/GebpuB
---
`Novo Produto` - https://wireframe.cc/UAwEvN
---
`Listar Produtos` - https://wireframe.cc/uozghL
---
`Novo Funcionário` - https://wireframe.cc/tECK2J
---
`Listar Funcionários` - https://wireframe.cc/1cADu1
---
`Nova Mesa` - https://wireframe.cc/UAwEvN
---
`Listar Mesas` - https://wireframe.cc/4gE6LQ
---
Diagrama de Classes
----------------------------

![Diagrama de Classes](resources/Diagrama%20de%20Classes.png)

