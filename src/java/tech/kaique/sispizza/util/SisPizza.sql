create table Usuario(
    idUsuario serial not null primary key,
    nomeUsuario varchar,
    cpfUsuario varchar,
    emailUsuario varchar,
    senhaUsuario varchar
);

create table Endereco(
    idEndereco serial not null primary key,
    nomeEndereco varchar,
    logradouroEndereco varchar,
    bairroEndereco varchar,
    cidadeEndereco varchar,
    ufEndereco varchar,
    cepEndereco varchar,
    principal boolean,
    idUsuario integer references Usuario(idUsuario)
);

create table Permissao(
    idPermissao serial not null primary key,
    descricaoPermissao varchar
);

create table Funcionario(
    idFuncionario serial not null primary key,
    nomeFuncionario varchar,
    emailFuncionario varchar,
    senhaFuncionario varchar,
    idPermissao integer references Permissao(idPermissao)
);

create table Mesa(
    idMesa serial not null primary key,
    qtdCadeirasMesa Integer,
    statusMesa boolean,
    localMesa varchar(1)
);

create table Reserva(
    idReserva serial not null primary key,
    statusReserva char(1),
    dataHoraReserva timestamp,
    idUsuario integer references Usuario(idUsuario),
    idMesa integer references Mesa(idMesa)
);

create table Produto(
    idProduto serial not null primary key,
    nomeProduto varchar,
    descricaoProduto varchar,
    valorProduto numeric,
    imagemProduto varchar
);

create table Pedido(
    idPedido serial not null primary key,
    statusPedido boolean,
    dataHoraPedido timestamp,
    tipoPedido char(1),
    idEntregador integer references Funcionario(idFuncionario),
    idReserva integer references Reserva(idReserva),
    idUsuario integer references Usuario(idUsuario),
    idMesa integer references Mesa(idMesa),
    idEndereco integer references Endereco(idEndereco),
    subTotalPedido numeric,
    descontoPedido numeric,
    totalPedido numeric
);

create table ItemPedido(
    idItemPedido serial not null primary key,
    idProduto integer references Produto(idProduto),
    idPedido integer references Pedido(idPedido),
    qtdItemPedido integer,
    subtotal numeric
);