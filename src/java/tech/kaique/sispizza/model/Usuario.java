package tech.kaique.sispizza.model;

import java.util.List;

public class Usuario {

    private Integer idUsuario;
    private String nomeUsuario;
    private String cpfUsuario;
    private String emailUsuario;
    private String senhaUsuario;
    private List<Endereco> enderecoUsuariolist;

    public Usuario() {
    }

    public Usuario(Integer idUsuario, String nomeUsuario, String cpfUsuario, String emailUsuario, String senhaUsuario, List<Endereco> enderecoUsuariolist) {
        this.idUsuario = idUsuario;
        this.nomeUsuario = nomeUsuario;
        this.cpfUsuario = cpfUsuario;
        this.emailUsuario = emailUsuario;
        this.senhaUsuario = senhaUsuario;
        this.enderecoUsuariolist = enderecoUsuariolist;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNomeUsuario() {
        return nomeUsuario;
    }

    public void setNomeUsuario(String nomeUsuario) {
        this.nomeUsuario = nomeUsuario;
    }

    public String getCpfUsuario() {
        return cpfUsuario;
    }

    public void setCpfUsuario(String cpfUsuario) {
        this.cpfUsuario = cpfUsuario;
    }

    public String getEmailUsuario() {
        return emailUsuario;
    }

    public void setEmailUsuario(String emailUsuario) {
        this.emailUsuario = emailUsuario;
    }

    public String getSenhaUsuario() {
        return senhaUsuario;
    }

    public void setSenhaUsuario(String senhaUsuario) {
        this.senhaUsuario = senhaUsuario;
    }

    public List<Endereco> getEnderecoUsuariolist() {
        return enderecoUsuariolist;
    }

    public void setEnderecoUsuariolist(List<Endereco> enderecoUsuariolist) {
        this.enderecoUsuariolist = enderecoUsuariolist;
    }

}
